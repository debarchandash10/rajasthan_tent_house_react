import React from 'react'
import "./index.css"

export default function Footer() {
  return (
    <div className='footer-main-wrapper'>
      <div className='footer-first-child'>
        <div className='footer-section'>
          <h5>About Us</h5>
          <p>We're Tilia, a team of photographers & videographers. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Est sale definitiones id. Ut quo quem harum munere, eu labore voluptatum mei.</p>
        </div>
        <div className='footer-section'>
          <h5>Contact Us</h5>
          <span>Location</span>
          <span>mobile</span>
          <span>email</span>
        </div>
        <div className='footer-section'>
          <h5>Quick Links</h5>
          <a href='#'>Home</a>
          <a href='#'>About</a>
          <a href='#'>Portfolio</a>
          <a href='#'>FAQ</a>
          <a href='#'>Contact</a>
        </div>
        <div className='footer-section'>
          <h5>Follow</h5>
          <a href='#'>Facebook</a>
          <a href='#'>Twitter</a>
          <a href='#'>Instagram</a>
          <a href='#'>Youtube</a>
          <a href='#'>Vimeo</a>
        </div>
      </div>
      <div className="footer-copy">
        <span className="footer-year">2022</span> © Tilia. Designed by MatchThemes.
      </div>
    </div>
  )
}
