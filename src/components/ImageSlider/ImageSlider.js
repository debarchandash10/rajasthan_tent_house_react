import React from 'react'
import imagesUrl from '../../contents/sliderImages.json'
import './index.css'

export default function ImageSlider() {
  return (
    <div className='main-wrapper'>
      <div className='image-row'>
        {
          imagesUrl.images.map((curValue, index) => {
            return (
              <img key={index} className='image-item' src={curValue} alt='pic' />
            )
          })
        }
      </div>
    </div>
  )
}
