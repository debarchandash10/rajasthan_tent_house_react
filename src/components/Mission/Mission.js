import React from 'react'
import backgroundImage2 from '../../assets/images/003-vmake.jpg';
import './mission.css'


export default function Mission() {
    return (
        <section className="py-5 service-page">
            <div className="mission-container">
                <div className="mission-row">
                    <div className="col-12">
                        <div className="heading-body">
                            <h2 className="mission-heading">
                                <span>Our Mission</span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div className="service-body">
                    <div className="mission-row mission-row-content">
                        <div className="mission-col misson-img-col">
                            <div className="mission-img">
                                <img src={backgroundImage2} alt="" height={450} className="about-mission-img"/>
                            </div>
                        </div>
                        <div className="mission-col">
                            <div className="mission-text">
                                <p>
                                    Rajasthan Tent House is the one of the best tent house in Banswara whome giving best services to our customers scince 2000. We are looking all new genration and modern services for Marriage function, Birthday Parties, Conferences, Cocktail, Religious Functions &amp; other auspicious functions. Enriched experience of 15+ years Rajasthan Tent House have handled all kinds of fuctions and memoriable event of your life . Rajasthan Tent House have offered history of mesmerising décor, fabulous food and different venues at you near location . Rajasthan Tent House give their clients a variety in their mandaps like Ac/Waterproof marriage mandaps.
                                </p>

                                <p>We provides to A to Z solutions for your happiest moment of to our clients with quality work. We regularly update the variety of pandals as per the requirement of our clients so that we can fulfill requirement of our each customer and provide them with the best pandal with new looks with your color choice . Rajasthan Tent House have designed the events with distinction &amp; dedication which make unique to othes . Light up your life, make your day colourful &amp; remember this day, as the brightest day of your life.We Provide some elements of your life Rose Crstal, Mudrin Duck, Goden Fish ,and many more ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    )
}
