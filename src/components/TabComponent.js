import React from 'react'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import './tab.css';
import tabNames from "../contents/tabs.json"
import tabContents from '../contents/tabContents.json'
import ServicesGallery from './ServicesGallery/ServicesGallery';

export default function TabComponent() {
    return (
        <div>
            <Tabs>
                <TabList>
                    {
                        tabNames.tabs.map((curValue,ind) => {
                            return (<Tab key={ind}><u>{curValue}</u></Tab>)
                        })
                    }
                </TabList>
                {
                    tabContents.contents.map((curValue,index)=>{
                        return(<TabPanel key={index}><ServicesGallery/></TabPanel>)
                    })
                }

            </Tabs>
        </div>
    )
}
