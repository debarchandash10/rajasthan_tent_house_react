import React, { useState, useEffect } from 'react';
import useWindowWidth from '../../utils/Hook/useWindowWidth';
import { FiGrid } from "react-icons/fi";
import { AiOutlineDoubleLeft } from "react-icons/ai";
import { FaSquareFacebook, FaSquareXTwitter, FaSquareInstagram, FaSquareYoutube } from "react-icons/fa6";
import './navbar.css'
import rjtlogo from "../../assets/images/rjt.png"
const Navbar = () => {
  const [scroll, setScroll] = useState(false);
  const screenWidth = useWindowWidth();
  const [menuDrawer, setMenuDrawer] = useState(false)
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 0) {
        setScroll(true);
      } else {
        setScroll(false);
      }
    };

    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  const changeMenudrawer = () => {
    setMenuDrawer(!menuDrawer);
    return;
  }

  return (
    <>
      {menuDrawer ?
        <div className='mob-nav-drawer-open'>
          <span className='nav-close-wrapper' onClick={changeMenudrawer}><AiOutlineDoubleLeft style={{ fontSize: 30 }} /></span>
          <div className='mob-nav-links-wrapper'>
            <ul className='mob-nav-links'>
              <li><a href="/">Home</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="/about">About</a></li>
              <li><a href="#">Portfolio</a></li>
            </ul>
          </div>

          <div className='nav-follow-section'>
            <a className='nav-follow-link' href='#'><FaSquareFacebook style={{ fontSize: 30 }} /></a>
            <a className='nav-follow-link' href='#'><FaSquareXTwitter style={{ fontSize: 30 }} /></a>
            <a className='nav-follow-link' href='#'><FaSquareInstagram style={{ fontSize: 30 }} /></a>
            <a className='nav-follow-link' href='#'><FaSquareYoutube style={{ fontSize: 30 }} /></a>
          </div>
        </div>
        :
        <nav className={scroll ? 'navbar scrolled' : 'navbar'}>
          {screenWidth > 750 ? <>
            <ul className="nav-links">
              <li><a href="#">Home</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="/about">About</a></li>
              <li><a href="#">Portfolio</a></li>
            </ul>
            {!scroll && (
              <button className="booknow-button">BookNow</button>
            )}
          </>
            :
            <div className='mob-nav'>
              <div className="mobile-img-wrapper"><img src={rjtlogo} height={100} width={100}/></div>
              <span className='menu-bg' onClick={changeMenudrawer}>
                <FiGrid style={{ fontSize: 30 }} />
              </span>
            </div>
          }
        </nav>
      }
    </>
  );
};

export default Navbar;