import React, { useState, useEffect } from 'react';
import { GoArrowRight, GoArrowLeft } from "react-icons/go";


import './index.css';
import useWindowWidth from '../../utils/Hook/useWindowWidth';

export default function TopImageSlider(props) {
    const [activeSlide, setActiveSlide] = useState(0);
    const windowWidth = useWindowWidth();
    const images = props.images;

    useEffect(() => {
        const interval = setInterval(() => {
            setActiveSlide((prevSlide) => (prevSlide + 1) % images.length);
        }, 5000);

        return () => clearInterval(interval);
    }, [images.length]);

    const handlePrevSlide = () => {
        setActiveSlide((prevSlide) => (prevSlide - 1 + images.length) % images.length);
    };

    const handleNextSlide = () => {
        setActiveSlide((prevSlide) => (prevSlide + 1) % images.length);
    };

    return (
        <div className="image-wrapper">
            <div className="slider">
                {images.map((image, index) => (
                    <div
                        key={index}
                        className={`slide ${index === activeSlide ? 'active' : ''}`}
                        style={{ opacity: index === activeSlide ? 1 : 0 }}
                    >
                        {windowWidth>450 ?
                            <img className="bg-img" src={image} alt={`background-pic-${index}`} />
                            :
                            <img className="bg-img" src={image} alt={`background-pic-${index}`} width={450} height={350}  />

                        }
                        {/* <div className="slide-controls">
                            <button className="prev-btn" onClick={handlePrevSlide}>
                                <GoArrowLeft/>
                            </button>
                            <button className="next-btn" onClick={handleNextSlide}>
                                <GoArrowRight/>
                            </button>
                        </div> */}
                    </div>
                ))}
            </div>
        </div>
    );
}