//This is Services File
import React from 'react'
import './index.css'
import tabContents from '../../contents/tabContents.json'

export default function ServicesGallery() {
    return (
        <div className='ig-wrapper'>
            <div className='ig-main'>
                {
                    tabContents.contents.map((curValue, index) => {
                        return (
                            <div key={index} className='card-wrapper'>
                                <div className='card-img-wrapper' >
                                    <img className='card-image'  src={curValue.url}  alt='image' />
                                </div>
                                <span className='card-title'>{curValue.title}</span>
                                <span className='card-category'>{curValue.category}</span>
                            </div>)
                    })
                }
            </div>
        </div>
    )
}
