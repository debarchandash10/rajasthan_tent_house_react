import React from 'react'
import './about.css'
import Navbar from '../../components/Navbar/Navbar'
import Footer from '../../components/Footer/Footer'
import TopImageSlider from '../../components/topImageSlider/TopImageSlider'
import backgroundImage5 from '../../assets/images/007-vmake.jpg';
import Scard from '../../components/Cards/ServicesDesc/SCard'
import ImageSlider from '../../components/ImageSlider/ImageSlider'
import { Tab } from '@headlessui/react'
import TabComponent from '../../components/TabComponent'
import Mission from '../../components/Mission/Mission'


export default function About() {
    const images = [
        backgroundImage5
    ];
    return (
        <div className='main-container'>
            <Navbar />
            <TopImageSlider images={images} />
            <div className='top-container'>
                <div className='heading-text-wrapper'>
                    <h1 className='top-heading-h1'>About Us</h1>
                    <span className='top-text-span'>Featured Photo Shoots</span>
                </div>
            </div>
            
            <div className='about-middle-container'>
                <Mission/>
                <TabComponent/>
            </div>
            <div className='home-slider'>
                <ImageSlider />
            </div>
            <div className='footer-wrapper'>
                <Footer />
            </div>
        </div>
    )
}
