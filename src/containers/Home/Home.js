import React from 'react'
import './index.css'
//home background image
import TabComponent from '../../components/TabComponent'
import ImageSlider from '../../components/ImageSlider/ImageSlider'
import Footer from '../../components/Footer/Footer'
import Navbar from '../../components/Navbar/Navbar'
import TopImageSlider from '../../components/topImageSlider/TopImageSlider'
import backgroundImage1 from '../../assets/images/002-vmake.jpg';
import backgroundImage2 from '../../assets/images/003-vmake.jpg';
import backgroundImage3 from '../../assets/images/005-vmake.jpg';
import backgroundImage4 from '../../assets/images/006-vmake.jpg';
import backgroundImage5 from '../../assets/images/007-vmake.jpg';
import backgroundImage6 from '../../assets/images/001.jpg';

export default function Home() {
    const images = [
        backgroundImage1,
        backgroundImage2,
        backgroundImage3,
        backgroundImage4,
        backgroundImage5,
        backgroundImage6
    ];
    return (
        <div className='main-container'>
            <Navbar />
            <TopImageSlider images={images}/>
            <div className='top-container'>
                <div className='heading-text-wrapper'>
                    {/* <h1 className='top-heading-h1'>Portfolio Grid v1 3 Cols</h1>
                    <span className='top-text-span'>Featured Photo Shoots</span> */}
                </div>
            </div>
            <div className='middle-content-container'>
                <TabComponent />
            </div>
            <div className='home-slider'>
                <ImageSlider />
            </div>
            <div className='footer-wrapper'>
                <Footer />
            </div>
        </div>
    )
}
